import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
// import {APP_BASE_HREF, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { foodIonicApp } from './app.component';

import { PipesModule } from '../src/pipes/pipes.module';

import {PlantService} from "../src/providers/plant-service-rest";
import {CampainService} from '../src/providers/campain-service-rest';
import {CategoryService} from "../src/providers/category-service";
import {LoginService} from "../src/providers/login-service";
import {CartService} from "../src/providers/cart-service-mock";
import {OrdersService} from "../src/providers/orders-service-mock";
import {MessageService} from "../src/providers/message-service-rest";
import {UserService} from '../src/providers/user-service-rest';

@NgModule({
  declarations: [
    foodIonicApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(foodIonicApp, {
			preloadModules: true,
			iconMode: 'md',
			mode: 'md'
    }),
    IonicStorageModule.forRoot({
      name: '__foodIonicDB',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    foodIonicApp
  ],
  providers: [
	  PlantService,
	  CampainService,
	  CategoryService,
	  LoginService,
	  MessageService,
	  CartService,
	  OrdersService,
	  UserService,
    // { provide: LocationStrategy, useClass: PathLocationStrategy },
    // { provide: APP_BASE_HREF, useValue : '/' },
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
