import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IonicPage, NavController, AlertController, ToastController, MenuController } from 'ionic-angular';

import { LoginService } from "../../providers/login-service";

@IonicPage({
  name: 'page-auth',
  segment: 'auth',
  priority: 'high'
})

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html'
})
export class AuthPage implements OnInit {
  public onLoginForm: FormGroup;
  public onRegisterForm: FormGroup;
  auth: string = "login";

  constructor(private _fb: FormBuilder, public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController, public toastCtrl: ToastController, public service: LoginService) {
    this.menu.swipeEnable(false);
    this.menu.enable(false);
  }

  ngOnInit() {
    this.onLoginForm = this._fb.group({
      email: ['', Validators.compose([
        Validators.required
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])]
    });

    this.onRegisterForm = this._fb.group({
      firstName: ['', Validators.compose([
        Validators.required
      ])],
      lastName: ['', Validators.compose([
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.required
      ])],
      age: ['', Validators.compose([
        Validators.required
      ])],
      gender: ['', Validators.compose([
        Validators.required
      ])],
      username: ['', Validators.compose([
        Validators.required
      ])],
      password: ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  // login and go to home page
  login() {
    this.service.login(this.onLoginForm.value.email, this.onLoginForm.value.password)
      .subscribe((data: any) => {
        if (data.success > 0) {
          this.service.setCurrUser(data); this.nav.setRoot('page-home');
        }
      });
  }

  saveUser() {
    this.service.signUp(this.onRegisterForm.value)
      .subscribe(
        (data: any) => {
          if (data.success > 0) {
            this.service.login(this.onRegisterForm.value.username, this.onRegisterForm.value.password)
              .subscribe(data => { this.service.setCurrUser(data); this.nav.setRoot('page-home'); });
          }
          else {
              const toast = this.toastCtrl.create({
                message: data.errors.emil ? data.errors.emil : data.errors.username,
                duration: 3000
              });
              toast.present();
          }
        }
      );
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Şifreyi Unuttun?',
      message: "Lütfen, şifreyi yeniden oluşturma linkini göndermek için mail adresinizi giriniz.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Mail Adresiniz',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'İptal',
          handler: data => {
          }
        },
        {
          text: 'Gönder',
          handler: data => {
            let toast = this.toastCtrl.create({
              message: 'Mail başarıyla gönderildi.',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'Tamam',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
