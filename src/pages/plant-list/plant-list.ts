import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicPage, Config, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import leaflet from 'leaflet';

import { PlantService } from '../../providers/plant-service-rest';
import { CategoryService } from '../../providers/category-service';
import { LoginService } from '../../providers/login-service';

@IonicPage({
    name: 'page-plant-list',
    segment: 'plant-list'
})

@Component({
    selector: 'page-plant-list',
    templateUrl: 'plant-list.html'
})
export class PlantListPage {

    plants: Array<any>;
    searchKey: string = "";
    viewMode: string = "list";
    categories: Array<any>;
    proptype: string;
    from: string;
    map;
    markersGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, public service: PlantService,
        public catService: CategoryService, public toastCtrl: ToastController, public modalCtrl: ModalController,private authService: LoginService,
        public config: Config, public geolocation: Geolocation) {
        this.findAll();
        this.proptype = this.navParams.get('proptype') || "";
        this.from = this.navParams.get('from') || "";
        this.findAllCategories();
    }

    openFilterModal() {
        let modal = this.modalCtrl.create('page-plant-filter');
        // modal.onDidDismiss(data => {
        // });
        modal.present();
    }

    openPlantDetail(plant: any) {
        this.navCtrl.push('page-plant-detail', {
            'id': plant.id_store
        });
    }

    favorite(plant) {
        this.service.favorite(this.authService.getCurrUser() ? this.authService.getCurrUser().id_user : -1, plant.id_store)
            .subscribe(plant => {
                let toast = this.toastCtrl.create({
                    message: 'İşletme favorilerinize eklendi',
                    cssClass: 'mytoast',
                    duration: 2000
                });
                toast.present(toast);
            });
    }

    onInput(event) {
        this.service.findByName(this.searchKey)
            .then(data => {
                this.plants = data;
                if (this.viewMode === "map") {
                    this.showMarkers();
                }
            })
            .catch(error => alert(JSON.stringify(error)));
    }

    onCancel(event) {
        this.findAll();
    }

    findAll() {
        this.service.findAll()
            .subscribe(data => { this.plants = data; });
    }

    findAllCategories() {
        this.catService.findAll()
            .subscribe(data => { this.categories = data; });
    }

    showMap() {
        setTimeout(() => {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.map = leaflet.map("map").setView([resp.coords.latitude, resp.coords.longitude], 16);
                leaflet.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                    attribution: 'Tiles &copy; Esri'
                }).addTo(this.map);
                this.showMarkers();

            }, (err) => {
            });

        }, 200)
    }

    showMarkers() {
        if (this.markersGroup) {
            this.map.removeLayer(this.markersGroup);
        }
        this.markersGroup = leaflet.layerGroup([]);
        this.plants.forEach(plant => {
            if (plant.latitude, plant.longitude) {
                let marker: any = leaflet.marker([plant.latitude, plant.longitude]).on('click', event => this.openPlantDetail(event.target.data));
                marker.data = plant;
                this.markersGroup.addLayer(marker);
            }
        });
        this.map.addLayer(this.markersGroup);
    }

}
