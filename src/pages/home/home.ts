import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { IonicPage, NavController, AlertController, MenuController, ToastController, PopoverController, ModalController, Platform } from 'ionic-angular';

import { PlantService } from '../../providers/plant-service-rest';
import { MessageService } from '../../providers/message-service-rest';
import { LoginService } from "../../providers/login-service";
import { PlantListPage } from '../plant-list/plant-list';
import { BackgroundMode } from '@ionic-native/background-mode';
/* import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation'; */

@IonicPage({
	name: 'page-home',
	segment: 'home',
	priority: 'high'
})

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {

	plants: Array<any> = [];
	searchKey: string = "";
	yourLocation: string = "";
	currUser: any;
	currDiscussions: Array<any> = [];
	radiusmiles: Number = 1;

	constructor(public navCtrl: NavController, public menuCtrl: MenuController, public popoverCtrl: PopoverController,
		public locationCtrl: AlertController, public modalCtrl: ModalController, public toastCtrl: ToastController,
		public service: PlantService, public messageService: MessageService, public authService: LoginService,
		public localNotifications: LocalNotifications, public geolocation: Geolocation, private platform: Platform, private backgroundMode: BackgroundMode) {

		/* 	const config: BackgroundGeolocationConfig = {
				desiredAccuracy: 10,
				stationaryRadius: 20,
				distanceFilter: 30,
				debug: true, //  enable this hear sounds for background-geolocation life-cycle.
				stopOnTerminate: false, // enable this to clear background location settings when the app terminates
			}; */

		this.currUser = this.authService.getCurrUser();
		this.menuCtrl.swipeEnable(true, 'authenticated');
		this.menuCtrl.enable(true);
		this.findAll();
		this.backgroundMode.enable();

		this.platform.ready().then(() => {
			this.execMessageLocationControl();
			/* this.backgroundGeolocation.start();

			this.backgroundGeolocation.configure(config)
				.subscribe((location: BackgroundGeolocationResponse) => {
					this.execMessageLocationControl();
					console.log(location);
					this.backgroundGeolocation.finish();

				}); */

		});
	}

	openPlantListPage(proptype) {
		this.navCtrl.push('page-plant-list', proptype);
	}

	openPlantFilterPage() {
		let modal = this.modalCtrl.create('page-plant-filter');
		modal.onDidDismiss(data => {
		});
		modal.present();
	}

	openPlantPage() {
		this.navCtrl.push('page-plant-list');
	}

	openFavoritePage() {
		this.navCtrl.push('page-favorite-list');
	}

	openCart() {
		this.navCtrl.push('page-cart');
	}

	openPlantDetail(plant: any) {
		this.navCtrl.push('page-plant-detail', {
			'id': plant.id_store
		});
	}

	openSettingsPage() {
		this.navCtrl.push('page-settings');
	}

	openNotificationsPage() {
		this.navCtrl.push('page-notifications');
	}

	openCategoryPage() {
		this.navCtrl.push('page-category');
	}

	onInput(event) {
		this.service.findByName(this.searchKey)
			.then(data => {
				this.plants = data;
			})
			.catch(error => alert(JSON.stringify(error)));
	}

	onCancel(event) {
		this.findAll();
	}

	findAll() {
		this.service.findAll()
			.subscribe((data: any) => {
				this.plants = data;
				var datas = {
					address: "Küçükbakkalköy Mahallesi, Dudullu Osb/Ataşehir/İstanbul",
					category_id: "74",
					category_name: "Otel",
					city: null,
					customers: null,
					date_created: "2018-08-24",
					detail: "Boş",
					distance: "Hesaplanıyor",
					featured: "1",
					id_store: "9",
					images: {},
					latitude: "40.984929",
					longitude: "29.157680",
					name: "Sürücü Kursu",
					nbrOffers: 0,
					nbr_votes: "0",
					status: "1",
					tags: null,
					telephone: "",
					user: {
						success: 1, result: {
							age: "",
							blacklist: null,
							confirmed: "1",
							dateLogin: null,
							date_created: null,
							email: "burhangok@yahoo.com",
							gender: "",
							guest_id: "1",
							id_user: "2",
							images: null,
							is_online: "0",
							lat: "0",
							lng: "0",
							manager: "1",
							name: "BiBul",
							password: "95620dea4f9c8e23131e550b682321d180083e3c",
							senderid: null,
							status: "1",
							telephone: null,
							typeAuth: "admin",
							username: "admin"
						}
					},
					user_id: "2",
					voted: false,
					votes: 0
				}
				var datas2 = {
					address: "Mimar Sinan mahallesi, Çelebi Mehmet Paşa caddesi, Aybek sokak no:20/4 Ataşehir/İstanbul",
					category_id: "74",
					category_name: "Ev",
					city: null,
					customers: null,
					date_created: "2018-08-24",
					detail: "Boş",
					distance: "Hesaplanıyor",
					featured: "1",
					id_store: "9",
					images: {},
					latitude: "40.984043",
					longitude: "29.159131",
					name: "Çamlıca",
					nbrOffers: 0,
					nbr_votes: "0",
					status: "1",
					tags: null,
					telephone: "",
					user: {
						success: 1, result: {
							age: "",
							blacklist: null,
							confirmed: "1",
							dateLogin: null,
							date_created: null,
							email: "burhangok@yahoo.com",
							gender: "",
							guest_id: "1",
							id_user: "2",
							images: null,
							is_online: "0",
							lat: "0",
							lng: "0",
							manager: "1",
							name: "BiBul",
							password: "95620dea4f9c8e23131e550b682321d180083e3c",
							senderid: null,
							status: "1",
							telephone: null,
							typeAuth: "admin",
							username: "admin"
						}
					},
					user_id: "2",
					voted: false,
					votes: 0
				}
				this.plants.push(datas);
				this.plants.push(datas2);
				for (var plant of this.plants) {
					plant.distance = 'Hesaplanıyor';
				}

			});
	}

	alertLocation() {
		let changeLocation = this.locationCtrl.create({
			title: 'Change Location',
			message: "Type your Address to change plant list in that area.",
			inputs: [
				{
					name: 'location',
					placeholder: 'Enter your new Location',
					type: 'text'
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
					}
				},
				{
					text: 'Change',
					handler: data => {
						this.yourLocation = data.location;
						let toast = this.toastCtrl.create({
							message: 'Location was change successfully',
							duration: 3000,
							position: 'top',
							closeButtonText: 'Tamam',
							showCloseButton: true
						});
						toast.present();
					}
				}
			]
		});
		changeLocation.present();
	}

	presentNotifications(myEvent) {
		let popover = this.popoverCtrl.create('page-notifications');
		popover.present({
			ev: myEvent
		});
	}

	ionViewWillEnter() {
		this.navCtrl.canSwipeBack();
	}

	execMessageLocationControl() {
		setTimeout(() => {
			var distancePlants = [];
			var i = 0;
			this.geolocation.getCurrentPosition().then((resp) => {
				for (var plant of this.plants) {
					if (plant.latitude && plant.longitude) {
						plant.distance = this.calculateDistance(resp.coords, plant).toFixed(2) + ' km';
					}
					var m = (parseFloat(plant.distance) * 1000.000).toFixed(0);
					if (parseInt(m) < 15000) {
						i++;
						var plantName = plant.name + ' ' + m + ' m';
						distancePlants.push({
							id: i,
							title: 'Hoşgeldiniz',
							text: plantName,
							foreground: true,
							led: 'FF0000',
							data: plant,
							at: new Date(new Date().getTime() + 5 * 1000),
							actions: [{ id: 'go', title: 'Konuma git' }]
						});
						console.log(plantName);
						
						this.localNotifications.schedule(distancePlants);

						this.localNotifications.on('click').subscribe(data => {
							this.navCtrl.setRoot(PlantListPage, data);
						});
						this.currDiscussions = this.messageService.getDiscussions();
					}

				}
			});

			this.execMessageLocationControl();
		}, 3000);

	}

	toRad(value) {
		var RADIANT_CONSTANT = 0.0174532925199433;
		return (value * RADIANT_CONSTANT);
	}

	calculateDistance(starting, ending) {
		var KM_RATIO = 6371;
		try {
			var dLat = this.toRad(ending.latitude - starting.latitude);
			var dLon = this.toRad(ending.longitude - starting.longitude);
			var lat1Rad = this.toRad(starting.latitude);
			var lat2Rad = this.toRad(ending.latitude);

			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1Rad) * Math.cos(lat2Rad);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var d = KM_RATIO * c;
			return d;
		} catch (e) {
			return -1;
		}
	}

	messageChangedControl(tmp) {
		if (tmp.length != this.currDiscussions)
			return true;
		else {
			for (let i = 0; i < tmp.length; i++) {
				if (tmp.messages.status === 0 && this.currDiscussions[0].messages.status === 1)
					return true;
			}
		}
		return false;
	}

}
