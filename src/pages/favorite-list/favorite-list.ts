import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {PlantService} from '../../providers/plant-service-rest';
import {LoginService} from "../../providers/login-service";

@IonicPage({
	name: 'page-favorite-list',
	segment: 'favorites'
})

@Component({
    selector: 'page-favorite-list',
    templateUrl: 'favorite-list.html'
})

export class FavoriteListPage {

    favorites: Array<any> = [];

    constructor(public navCtrl: NavController, public authService: LoginService, public service: PlantService) {
        this.getFavorites();
    }

    itemTapped(favorite) {
		this.navCtrl.push('page-plant-detail', {
			'id': favorite.id_store
		});
    }

    deleteItem(favorite) {
        this.service.unfavorite(favorite)
			.subscribe((data:any) => {
				this.favorites = data.result;
			});
    }

    getFavorites() {
        this.service.getFavorites(this.authService.getCurrUser() ? this.authService.getCurrUser().id_user : -1)
			.subscribe((data:any) => {
				this.favorites = data.result;
				for (var i=0; i < this.favorites.length; i++) {
					this.favorites[i].images = JSON.parse('[' + JSON.stringify(this.favorites[i].images) + ']');
					this.favorites[i].images[0] = JSON.parse('[' + this.favorites[i].images[0] + ']')[0][0];
				}
			});
    }

}
