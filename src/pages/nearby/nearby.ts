import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { PlantService } from '../../providers/plant-service-rest';

import leaflet from 'leaflet';

@IonicPage({
    name: 'page-nearby',
    segment: 'nearby'
})

@Component({
    selector: 'page-nearby',
    templateUrl: 'nearby.html'
})
export class NearbyPage {

    plants: Array<any>;
    map;
    markersGroup;

    constructor(public navCtrl: NavController, public service: PlantService, public modalCtrl: ModalController) {
        this.findAll();
    }

    openRestaurantFilterPage() {
        let modal = this.modalCtrl.create('page-restaurant-filter');
        // modal.onDidDismiss(data => {
        // });
        modal.present();
    }

    openRestaurantDetail(plant: any) {
        this.navCtrl.push('page-plant-detail', {
            'id': plant.id
        });
    }

    findAll() {
        this.service.findAll()
            .subscribe(data => this.plants = data);
    }

    showMarkers() {
        if (this.markersGroup) {
            this.map.removeLayer(this.markersGroup);
        }
        this.markersGroup = leaflet.layerGroup([]);
        this.plants.forEach(plant => {
            if (plant.lat, plant.long) {
                let marker: any = leaflet.marker([plant.lat, plant.long]).on('click', event => this.openRestaurantDetail(plant));
                marker.data = plant;
                this.markersGroup.addLayer(marker);
            }
        });
        this.map.addLayer(this.markersGroup);
    }

    ionViewDidLoad() {
        setTimeout(() => {
            this.map = leaflet.map("nearby-map").setView([42.361132, -71.070876], 14);
            leaflet.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri'
            }).addTo(this.map);
            this.showMarkers();
        })
    }

}
