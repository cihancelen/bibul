import { Component } from '@angular/core';
import { IonicPage, ActionSheetController, ActionSheet, NavController, NavParams, ToastController, PopoverController } from 'ionic-angular';

import { PlantService } from '../../providers/plant-service-rest';
import { CampainService } from '../../providers/campain-service-rest';
import { LoginService } from "../../providers/login-service";

import leaflet from 'leaflet';
import { MessageService } from '../../providers/message-service-rest';

@IonicPage({
    name: 'page-plant-detail',
    segment: 'plant/:id'
})

@Component({
    selector: 'page-plant-detail',
    templateUrl: 'plant-detail.html'
})
export class PlantDetailPage {
    param: number;

    map;
    markersGroup;
    plant: any;
    plants: Array<any> = [];
    restaurantopts: String = 'menu';
    campains: Array<any> = [];
    currUser: any;
    discussion: any;
    constructor(public actionSheetCtrl: ActionSheetController, private messageService: MessageService, private authServ: LoginService, public navCtrl: NavController, public navParams: NavParams, public plantService: PlantService, public campainService: CampainService, public toastCtrl: ToastController, public authService: LoginService) {
        this.param = this.navParams.get('id');
        this.plantService.findAll().subscribe((response: any) => {
            this.plants = response;
            this.plant = this.plants.find(item => item.id_store === this.param);
            this.findAllCampains();
        });
        this.currUser = this.authServ.getCurrUser();
    }

    openCampainDetail(campain) {
        this.campainService.setCampain(campain);
        this.navCtrl.push('page-campain-detail');
    }

    findAllCampains() {
        this.campainService.findAll(this.param)
            .subscribe((data: any) => {
                var campains = data.result;
                if (campains.length > 0) {
                    campains.forEach(item => {
                        item.content = JSON.parse('[' + item.content + ']');
                        this.campains.push(item);
                    });
                }
            });
    }

    favorite() {
        this.plantService.favorite(this.authService.getCurrUser() ? this.authService.getCurrUser().id_user : -1, this.param)
            .subscribe(plant => {
                let toast = this.toastCtrl.create({
                    message: 'İşletme favorilerinize eklendi',
                    cssClass: 'mytoast',
                    duration: 2000
                });
                toast.present(toast);
            });
    }

    share(plant) {
        let actionSheet: ActionSheet = this.actionSheetCtrl.create({
            title: 'Share via',
            buttons: [
                {
                    text: 'Twitter',
                },
                {
                    text: 'Facebook',
                },
                {
                    text: 'Email',
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                }
            ]
        });

        actionSheet.present();
    }

    openCart() {
        this.navCtrl.push('page-cart');
    }

    showMarkers() {

        let marker: any = leaflet.marker([this.plant.latitude, this.plant.longitude]);
        //marker.data = this.plant;
        marker.addTo(this.map)
            .bindPopup(this.plant.address)
            .openPopup();
    }

    showMap() {
        setTimeout(() => {
            this.map = leaflet.map("map-detail").setView([this.plant.latitude, this.plant.longitude], 16);
            leaflet.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri'
            }).addTo(this.map);
            this.showMarkers();
        }, 200)
    }

    message() {
        this.discussion = [];
        this.messageService.findByUserId(this.currUser ? this.currUser.id_user : -1)
            .subscribe((data: any) => {

                for (let res in data.result) {
                    if (data.result[res].messages) {
                        data.result[res].sender = JSON.parse('[' + data.result[res].sender + ']');
                        data.result[res].messages = JSON.parse('[' + data.result[res].messages + ']');
                        data.result[res].sender = data.result[res].sender[0].result[0];
                        data.result[res].messages = data.result[res].messages[0].result[0];
                        data.result[res].messages.status = parseInt(data.result[res].messages.status);
                        if (data.result[res].messages.sender_id === this.plant.user.result[0].id_user || data.result[res].messages.receiver_id === this.plant.user.result[0].id_user) {
                            this.discussion = data.result[res];
                        }
                    }
                }

                this.messageService.loadMessages(this.discussion, this.currUser ? this.currUser.id_user : -1)
                    .subscribe((data: any) => {
                        let messages = [];
                        for (var i = 0; i < data.count; i++) {
                            var element = data.result[i];
                            messages.push(element);
                        }
                        this.messageService.assignMessages(messages);

                        this.discussion = { id_discussion: -1, sender: this.plant.user.result[0], sender_id: this.plant.user.result[0].id_user, receiver_id: this.plant.user.result[0].id_user };
                        let discussions = [];
                        discussions.push(this.discussion);
                        this.messageService.assignDiscussions(discussions);
                        
                        this.navCtrl.push('page-message-detail', {
                            'id': this.discussion.id_discussion
                        });
                    });

            });
    }

}
