import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {CategoryService} from '../../providers/category-service';

@IonicPage({
	name: 'page-category',
	segment: 'category'
})

@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  private categories : Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: CategoryService) {
  	this.findAll();
  }

  ionViewDidLoad() {
  }

  findAll() {
	this.service.findAll()
		.subscribe(data => {this.categories = data;});
  }

  openPlantListPage(proptype) {
  	this.navCtrl.push('page-plant-list', {'proptype': proptype});
  }

}
