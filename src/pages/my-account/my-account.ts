import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController } from 'ionic-angular';

import {LoginService} from "../../providers/login-service";

@IonicPage({
	name: 'page-my-account',
	segment: 'my-account'
})

@Component({
    selector: 'page-my-account',
    templateUrl: 'my-account.html'
})
export class MyAccountPage {

  profiledata: Boolean = true;
  currUser: any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public authService: LoginService) {
	  this.currUser = this.authService.getCurrUser();
  }

  // process send button
  sendData() {
    // send booking info
    let loader = this.loadingCtrl.create({
      content: "Lütfen Bekleyiniz..."
    });
    // show message
    let toast = this.toastCtrl.create({
      showCloseButton: true,
      cssClass: 'profiles-bg',
      message: 'Bilgileriniz Güncellendi!',
      duration: 3000,
      position: 'bottom',
      closeButtonText: 'Kapat'
    });

    loader.present();

    setTimeout(() => {
      loader.dismiss();
      toast.present();
      // back to home page
      this.navCtrl.setRoot('page-home');
    }, 3000)
  }

}
