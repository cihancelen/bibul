import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlantFilterPage } from './plant-filter';

@NgModule({
	declarations: [
		PlantFilterPage
	],
	imports: [
		IonicPageModule.forChild(PlantFilterPage)
	],
	exports: [
		PlantFilterPage
	]
})

export class PlantFilterPageModule { }
