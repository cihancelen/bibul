import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';

@IonicPage({
	name: 'page-plant-filter',
	segment: 'plant-filter'
})

@Component({
  selector: 'page-plant-filter',
  templateUrl: 'plant-filter.html',
})

export class PlantFilterPage {
	minmaxprice: any;
	radiusmiles: Number;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {

  	this.radiusmiles = 1;

		this.minmaxprice = {
  		upper:500,
  		lower:10
  	};

  }

  closeModal() {
    this.navCtrl.pop();
  }

	dismiss() {
		let data = { 'minmaxprice': this.minmaxprice, 'radiusmiles': this.radiusmiles };
		this.viewCtrl.dismiss(data);
	}

}
