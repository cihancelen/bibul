import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import {MessageService} from '../../providers/message-service-rest';
import {LoginService} from "../../providers/login-service";

@IonicPage({
	name: 'page-message-detail',
	segment: 'message/:id'
})

@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html'
})

export class MessageDetailPage implements OnInit {
	
	param: number;
	messages: Array<any> = [];
	currUser: any;
	currDiscussion: any;
	content: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public service: MessageService, public authService: LoginService, private toastController: ToastController) {
	console.log('consturctor');
	this.currUser = this.authService.getCurrUser();
	this.param = this.navParams.get('id');
	this.currDiscussion = this.service.getItem(this.param);
	this.messages = this.service.messages;
  }

  ngOnInit(): void {
	  console.log(this.messages);
}

  sendMessage() {
		if(this.content && this.content.length)
			this.service.sendMessage(this.currUser.id_user,this.currDiscussion, this.content)
				.subscribe((data:any) => { 
					this.content = '';
					 const toast = this.toastController.create({
						message: 'Mesaj gönderildi.',
						duration: 1000
					});
					toast.present();
					for(let res in data.result) {
						this.messages.push(data.result[res]);
					}
				},(error: any) => {
					console.log(error);
				});
  }

	execMessageLocationControl(){

		setTimeout(() => {

			this.service.loadMessages(this.currDiscussion, this.currUser ? this.currUser.id_user : -1)
				.subscribe((data:any) => {
					let tmpMessages = [];
					for (var i = 0; i < data.count; i++) {
						var element = data.result[i];
						tmpMessages.push(element);
					}
					this.service.assignMessages(tmpMessages);
					this.messages = this.service.getMessages();
				});

			if(this.currDiscussion )
				this.execMessageLocationControl();
		}, 3000);

	}

}
