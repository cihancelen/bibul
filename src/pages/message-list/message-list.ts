import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { MessageService } from '../../providers/message-service-rest';
import { LoginService } from "../../providers/login-service";

@IonicPage({
	name: 'page-message-list',
	segment: 'message-list'
})

@Component({
	selector: 'page-message-list',
	templateUrl: 'message-list.html'
})
export class MessageListPage {

	discussions: Array<any> = [];
	currUser: any;

	constructor(public navCtrl: NavController, public service: MessageService, public authService: LoginService) {
		this.currUser = this.authService.getCurrUser();
		this.getMessages();
	}

	itemTapped(discussion) {
		if (discussion.messages[0].status <= 0) {
			this.service.readedByUser(discussion.messages[0].receiver_id, discussion.id_discussion)
				.subscribe((data: any) => { });
			discussion.messages[0].status = 1;
		}

		this.service.loadMessages(discussion, this.currUser ? this.currUser.id_user : -1)
			.subscribe((data: any) => {
				let messages = [];
				for (var i = 0; i < data.count; i++) {
					var element = data.result[i];
					messages.push(element);
				}

				this.service.assignMessages(messages);

				this.navCtrl.push('page-message-detail', {
					'id': discussion.id_discussion
				});
			});
	}

	deleteItem(discussion) {
		this.service.delMessage(discussion);
	}

	getMessages() {
		var i = 0;
		this.service.findByUserId(this.currUser ? this.currUser.id_user : -1)
			.subscribe((data: any) => {
				var result = data.result;
				for (let index in result) {
					var item = result[index];
					if (item != undefined) {
						item.sender = JSON.parse('[' +item.sender + ']');
						item.messages = JSON.parse('[' + item.messages + ']');
						item.sender = item.sender[0].result;
						item.messages = item.messages[0].result;
						item.messages.status = parseInt(item.messages.status);
						this.discussions.push(item);

					}
				}
				this.service.assignDiscussions(this.discussions);
			});
	}

}
