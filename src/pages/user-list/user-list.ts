import { Component, LOCALE_ID } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";

import { UserService } from "../../providers/user-service-rest";
import { MessageService } from "../../providers/message-service-rest";
import { LoginService } from "../../providers/login-service";

import localeTr from "@angular/common/locales/tr";
import { registerLocaleData } from "@angular/common";

registerLocaleData(localeTr);

@IonicPage({
  name: "page-user-list",
  segment: "user-list"
})
@Component({
  selector: "page-user-list",
  templateUrl: "user-list.html",
  providers: [{ provide: LOCALE_ID, useValue: "tr" }]
})
export class UserListPage {
  users: Array<any> = [];
  currUser: any;

  constructor(
    public navCtrl: NavController,
    public userService: UserService,
    public messageService: MessageService,
    public authService: LoginService
  ) {
    this.currUser = this.authService.getCurrUser();
    this.findAll();
  }

  findAll() {
    this.userService.findAll().subscribe(data => {
      this.users = data;
    });
  }

  openMessageDetail(user) {
    this.messageService
      .findByUserId(this.currUser ? this.currUser.id_user : -1)
      .subscribe((data: any) => {
        let discussion: any;
        for (let res in data.result) {
          if (data.result[res].messages) {
            data.result[res].sender = JSON.parse(
              "[" + data.result[res].sender + "]"
            );
            data.result[res].messages = JSON.parse(
              "[" + data.result[res].messages + "]"
            );
            data.result[res].sender = data.result[res].sender[0].result[0];
            data.result[res].messages = data.result[res].messages[0].result[0];
            data.result[res].messages.status = parseInt(
              data.result[res].messages.status
            );
            if (
              data.result[res].messages.sender_id === user.id_user ||
              data.result[res].messages.receiver_id === user.id_user
            ) {
              discussion = data.result[res];
            }
          }
        }
        if (discussion)
          this.messageService
            .loadMessages(
              discussion,
              this.currUser ? this.currUser.id_user : -1
            )
            .subscribe((data: any) => {
              let messages = [];
              for (var i = 0; i < data.count; i++) {
                var element = data.result[i];
                messages.push(element);
              }
              this.messageService.assignMessages(messages);

              this.navCtrl.push("page-message-detail", {
                id: discussion.id_discussion
              });
            });
        else {
          discussion = {
            id_discussion: -1,
            sender: user,
            sender_id: user.id_user,
            receiver_id: user.id_user
          };
          let discussions = [];
          discussions.push(discussion);
          this.messageService.assignMessages([]);
          this.messageService.assignDiscussions(discussions);
          this.navCtrl.push("page-message-detail", {
            id: discussion.id_discussion
          });
        }
      });
  }
}
