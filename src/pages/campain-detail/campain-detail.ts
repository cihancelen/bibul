import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';

import {CampainService} from '../../providers/campain-service-rest';

@IonicPage({
	name: 'page-campain-detail',
	segment: 'campain'
})

@Component({
    selector: 'page-campain-detail',
    templateUrl: 'campain-detail.html'
})
export class CampainDetailPage {
	param: number;
	campain: any;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public campainService: CampainService) {
  	this.campain = this.campainService.getCurrCampain();
  }

}
