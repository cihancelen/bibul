import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CampainDetailPage } from './campain-detail';

@NgModule({
	declarations: [
		CampainDetailPage
	],
	imports: [
		IonicPageModule.forChild(CampainDetailPage)
	],
	exports: [
		CampainDetailPage
	]
})

export class CampainDetailPageModule { }
