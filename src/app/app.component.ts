import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import { MessageService } from '../providers/message-service-rest';
import { LoginService } from "../providers/login-service";

export interface MenuItem {
	title: string;
	component: any;
	icon: string;
}

@Component({
	templateUrl: 'app.html'
})
export class foodIonicApp {
	options: InAppBrowserOptions = {
		location: 'yes',//Or 'no'
		hidden: 'no', //Or  'yes'
		clearcache: 'yes',
		clearsessioncache: 'yes',
		zoom: 'yes',//Android only ,shows browser zoom controls
		hardwareback: 'yes',
		mediaPlaybackRequiresUserAction: 'no',
		shouldPauseOnSuspend: 'no', //Android only
		closebuttoncaption: 'Close', //iOS only
		disallowoverscroll: 'no', //iOS only
		toolbar: 'yes', //iOS only
		enableViewportScale: 'no', //iOS only
		allowInlineMediaPlayback: 'no',//iOS only
		presentationstyle: 'pagesheet',//iOS only
		fullscreen: 'yes',//Windows only
	};
	@ViewChild(Nav) nav: Nav;

	tabsPlacement: string = 'bottom';
	tabsLayout: string = 'icon-top';

	rootPage: any = 'page-auth';
	showMenu: boolean = true;

	homeItem: any;

	initialItem: any;

	messagesItem: any;

	settingsItem: any;

	appMenuItems: Array<MenuItem>;

	yourRestaurantMenuItems: Array<MenuItem>;

	accountMenuItems: Array<MenuItem>;

	helpMenuItems: Array<MenuItem>;

	constructor(public platform: Platform, public authService: LoginService, public messageService: MessageService, public theInAppBrowser: InAppBrowser) {
	
		this.initializeApp();

		this.homeItem = { component: 'page-home' };
		this.messagesItem = { component: 'page-message-list' };


		this.appMenuItems = [
			{ title: 'İşletmeler', component: 'page-plant-list', icon: 'home' },
			{ title: 'Yakınımdaki İnsanlar', component: 'page-user-list', icon: 'person' },
			{ title: 'Kategoriler', component: 'page-category', icon: 'albums' },
			{ title: 'Favori İşletmelerim', component: 'page-favorite-list', icon: 'heart' }
		];

		this.yourRestaurantMenuItems = [
			{ title: 'İşletmenizi Yönetin', component: 'page-your-restaurant', icon: 'clipboard' }
		];


		this.accountMenuItems = [
			{ title: 'Oturum Ekranı', component: 'page-auth', icon: 'log-in' },
			{ title: 'Hesabım', component: 'page-my-account', icon: 'contact' },
			{ title: 'Çıkış', component: 'page-auth', icon: 'log-out' },
		];

		this.helpMenuItems = [
			{ title: 'Hakkımızda', component: 'page-about', icon: 'information-circle' }
		];

	}

	initializeApp() {
		this.platform.ready().then(() => {
			
		});
		if (!this.platform.is('mobile')) {
			this.tabsPlacement = 'top';
			this.tabsLayout = 'icon-left';
		}
	}

	currUser() {
		let user = this.authService.getCurrUser();
		return user ? user.name : null;
	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	manageYourPlant(page) {
		this.theInAppBrowser.create("http://89.252.190.174/~rbmiycom/index.php/dashboard/user/login", "_blank", this.options);
		/*this.theInAppBrowser.on('loadstop').subscribe(() => {
			},err =>
		);
		this.theInAppBrowser.on('exit').subscribe(() => {
			}, err =>
		);*/
	}

	messageCount() {
		setTimeout(() => {
			this.messageService.findByUserId(this.currUser ? this.currUser[0].id_user : -1)
				.subscribe((data: any) => {
					let discussions = [];
					for (let res in data.result) {
						if (data.result[res].messages) {
							data.result[res].messages = JSON.parse('[' + data.result[res].messages + ']');
							data.result[res].messages = data.result[res].messages[0].result[0];
							data.result[res].messages.status = parseInt(data.result[res].messages.status);
							if (data.result[res].messages.status <= 0)
								discussions.push(data.result[res]);
						}
					}
					return discussions.length;
				});
		}, 10000);
		return 0;
	}
}
