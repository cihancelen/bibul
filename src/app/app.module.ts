import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Geolocation } from '@ionic-native/geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { APP_BASE_HREF, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { foodIonicApp } from './app.component';

import { PipesModule } from '../pipes/pipes.module';

import { PlantService } from "../providers/plant-service-rest";
import { CampainService } from '../providers/campain-service-rest';
import { CategoryService } from "../providers/category-service";
import { LoginService } from "../providers/login-service";
import { CartService } from "../providers/cart-service-mock";
import { OrdersService } from "../providers/orders-service-mock";
import { MessageService } from "../providers/message-service-rest";
import { UserService } from '../providers/user-service-rest';
import { BackgroundMode } from '@ionic-native/background-mode';

@NgModule({
  declarations: [
    foodIonicApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(foodIonicApp, {
      preloadModules: true,
      iconMode: 'md',
      mode: 'md'
    }),
    IonicStorageModule.forRoot({
      name: '__bibulDB',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    foodIonicApp
  ],
  providers: [
    PlantService,
    CampainService,
    CategoryService,
    LoginService,
    MessageService,
    CartService,
    OrdersService,
    UserService,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LocalNotifications,
    Geolocation,
    InAppBrowser,
    BackgroundMode
  ]
})
export class AppModule { }