import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from './config';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

let catURL = api.SERVER_URL + 'category/getCategories';

@Injectable()
export class CategoryService {

	constructor(public http: HttpClient) {
	}

	findAll(): Observable<any> {
		return this.http.get(catURL).pipe(
			map(this.extractData),
			catchError(this.handleError)
		);
	}


	private extractData(res: any) {
		let body = res.result || [], body2 = [];
		for (let ent in body) {
			body2.push(body[ent]);
		}
		return body2;
	}

	private handleError (error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const err = error || '';
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		return Observable.throw(errMsg);
	}

}
