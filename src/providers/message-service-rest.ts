import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from './config';

let discussionURL = api.SERVER_URL + 'messenger/loadDiscussion',
	messSeenURL = api.SERVER_URL + 'messenger/markMessagesAsSeen',
	messageURL = api.SERVER_URL + 'messenger/loadMessages',
	sendMessageURL = api.SERVER_URL + 'messenger/sendMessage';

let httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class MessageService {

  discussions: any = [];
  messages: any = [];

  constructor(public http: HttpClient) {
  }

  findByUserId(userId) {

	  let body = 'user_id=' + userId;

	  return this.http.post(discussionURL, body, httpOptions);
  }

  loadMessages(discus, userId) {

		let body = 'discussion_id=' + discus.id_discussion + '&user_id=' + userId + '&receiver_id=' + discus.receiver_id;

		return this.http.post(messageURL, body, httpOptions);
  }

  sendMessage(userId,discusion, content) {

		let body = 'sender_id=' + userId + '&receiver_id=' + (discusion.receiver_id != userId ? discusion.receiver_id : discusion.sender_id) + '&status=0&content=' + content;

		return this.http.post(sendMessageURL, body, httpOptions);
  }

  readedByUser(userId, discussionId) {

		let body = 'user_id=' + userId + '&discussionId=' + discussionId;

		return this.http.post(messSeenURL, body, httpOptions);
  }

  getMessages() {
    return this.messages;
  }

  getDiscussions() {
		return this.discussions;
  }

  assignMessages(messages) {
    this.messages = messages;
  }

  assignDiscussions(discussions) {
		this.discussions = discussions;
  }

  getItem(id) {
    for (var i = 0; i < this.discussions.length; i++) {
      if (this.discussions[i].id_discussion === id) {
        return this.discussions[i];
      }
    }
    return this.discussions && this.discussions.length ? this.discussions[0] : null;
  }

  delMessage(message) {
    this.messages.splice(this.messages.indexOf(message), 1);
  }

}
