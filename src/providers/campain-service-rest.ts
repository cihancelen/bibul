import { Injectable } from '@angular/core';
import { api } from './config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

let campainURL = api.SERVER_URL + 'offer/getOffers';

let httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class CampainService {

  campain: any;

  constructor(public http: HttpClient) {

  }

  // Server URL Api sample
  /////////////////////
  findAll(plantId) {
	  let body = 'store_id=' + plantId;

	  return this.http.post(campainURL, body, httpOptions);
  }

  getCurrCampain() {
    return this.campain;
  }

  // Countries API
  setCampain(campain) {
	this.campain = campain;
  }

  private extractData(res: any) {
		let body = res.result || [], body2 = [];
		for (let ent in body) {
			body2.push(body[ent]);
		}
		return body2;
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
