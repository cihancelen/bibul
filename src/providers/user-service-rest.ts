import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from './config';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

let userURL = api.SERVER_URL + 'user/getUsers';

let httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

let users: Array<any> = [];

@Injectable()
export class UserService {

	favoriteCounter: number = 0;
	favorites: Array<any> = [];

	constructor(public http: HttpClient) {
	}

	findAll(): Observable<any> {
		return this.http.get(userURL).pipe(
			map(this.extractData),
			catchError(this.handleError)
		);
	}

	getUser(id,plants) {
		for (var i = 0; i < plants.length; i++) {
			if (users[i].id_store === id) {
				return plants[i];
			}
		}
		return null;
	}

	private extractData(res: Response | any) {
		let body = res.result || [];
		for (let ent in body) {
			users.push(body[ent]);
		}
		return users;
	}

	private handleError (error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const err = error || '';
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		return Observable.throw(errMsg);
	}

}
