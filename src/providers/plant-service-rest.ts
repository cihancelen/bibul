import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from './config';
import { Observable } from 'rxjs/Observable';
import { map, tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class PlantService {
	plantsURL = api.SERVER_URL + 'store/getStores';
	favoritesURL = api.SERVER_URL + 'store/getFavStores/';
	saveFavoriteURL = api.SERVER_URL + 'store/saveStore';
	rmFavoriteURL = api.SERVER_URL + 'store/removeStore';

	httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
	};

	plants: Array<any> = [];
	constructor(public http: HttpClient) {
		this.findAll().subscribe((data: any) => { this.plants = data });
	}

	findAll(): Observable<any> {
		return this.http.get(this.plantsURL).pipe(
			map(this.extractData),
			catchError(this.handleError)
		);
	}

	findByName(searchKey: string) {
		let key: string = searchKey.toUpperCase();
		return Promise.resolve(this.plants.filter((plant: any) =>
			(plant.name + ' ' + plant.category_name + ' ' + plant.address + ' ' + plant.detail).toUpperCase().indexOf(key) > -1));
	}

	findById(id): Observable<number> {
		return this.http.get(this.plantsURL + '/' + id).pipe(
			map(this.extractData),
			catchError(this.handleError)
		);
	}

	getFavorites(userId) {
		let body = 'user_id=' + userId;

		return this.http.post(this.favoritesURL, body, this.httpOptions);
	}

	getItem(id) {
		this.findAll().subscribe(res => {
			var plant = null;
			this.plants = res;
			console.log(this.plants);
			plant = this.plants.find(item => item.id_store == id);

			console.log(plant);
			return plant ? plant : null;
		});
	}

	favorite(userId, plantId) {
		let body = 'user_id=' + userId + '&store_id=' + plantId;

		return this.http.post(this.saveFavoriteURL, body, this.httpOptions);
	}

	unfavorite(favorite) {
		let body = 'user_id=' + favorite.user_id + '&store_id=' + favorite.id_store;

		return this.http.post(this.rmFavoriteURL, body, this.httpOptions);
	}

	// Private
	private extractData(res: Response | any) {
		let body = res.result || [];
		this.plants = [];
		for (let ent in body) {
			this.plants.push(body[ent]);
		}
		return this.plants;
	}

	private handleError(error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const err = error || '';
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		return Observable.throw(errMsg);
	}

}
