import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { api } from './config';
import { Observable } from 'rxjs/Observable';
import { map, tap, catchError } from 'rxjs/operators';

let loginURL = api.SERVER_URL + 'user/signIn',
	signupURL = api.SERVER_URL + 'user/signup';

let httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
};

@Injectable()
export class LoginService {

	currUser:any;
	constructor(public http: HttpClient) {
	}

	login(user,pass) {
		let body = 'login=' + user + '&password=' + pass;

		return this.http.post(loginURL, body, httpOptions);
	}

	signUp(user) {
		let body = 'firstName=' + user.firstName + '&lastName=' + user.lastName + '&email=' + user.email +
			       '&age=' + user.age + '&gender=' + user.gender + '&username=' + user.username +
				   '&password=' + user.password + '&name=' + user.firstName + ' ' + user.lastName;
		return this.http.post(signupURL, body, httpOptions);
	}

	setCurrUser(user){
		this.currUser = user;
	}

	getCurrUser(){
		return this.currUser && this.currUser.result ? this.currUser.result[0] : null;
	}

	private extractData(res: Response) {
		let body = res;
		return body || {};
	}

	private handleError (error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const err = error || '';
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		return Observable.throw(errMsg);
	}

}
